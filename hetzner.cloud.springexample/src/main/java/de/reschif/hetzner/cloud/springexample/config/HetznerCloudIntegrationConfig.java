package de.reschif.hetzner.cloud.springexample.config;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import com.hetzner.cloud.api.ActionApi;
import com.hetzner.cloud.api.ImageApi;
import com.hetzner.cloud.api.IsoApi;
import com.hetzner.cloud.api.LocationApi;
import com.hetzner.cloud.api.ServerTypeApi;
import com.hetzner.cloud.api.client.invoker.ApiClient;
import com.hetzner.cloud.api.client.invoker.auth.ApiKeyAuth;

@Configuration
@ComponentScan({"com.hetzner.cloud.api.client.invoker"})
public class HetznerCloudIntegrationConfig {

  private static final Logger LOG = LoggerFactory.getLogger(HetznerCloudIntegrationConfig.class);

  /**
   * A real value should be provided by environment property HETZNER_CLOUD_API_KEY
   */
  @Value("${hetzner.cloud.api.key}")
  private String apiKeyValue;

  @PostConstruct
  private void init() {
    if ("change_me".equals(apiKeyValue)) {
      LOG.warn("###########################################");
      LOG.warn("No proper value for environment property HETZNER_CLOUD_API_KEY found.");
      LOG.warn("###########################################");
    }

    ApiKeyAuth petStoreAuth = (ApiKeyAuth) apiClient.getAuthentication("Bearer");
    petStoreAuth.setApiKey(apiKeyValue);
    petStoreAuth.setApiKeyPrefix("Bearer");

  }

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    // Do any additional configuration here
    return builder.build();
  }

  @Autowired
  private ApiClient apiClient;

  @Bean
  public ActionApi actionApi() {
    return new ActionApi(apiClient);
  }

  @Bean
  public ImageApi imageApi() {
    return new ImageApi(apiClient);
  }

  @Bean
  public IsoApi isoApi() {
    return new IsoApi(apiClient);
  }

  @Bean
  public LocationApi locationApi() {
    return new LocationApi(apiClient);
  }

  @Bean
  public ServerTypeApi serverTypeApi() {
    return new ServerTypeApi(apiClient);
  }

}
