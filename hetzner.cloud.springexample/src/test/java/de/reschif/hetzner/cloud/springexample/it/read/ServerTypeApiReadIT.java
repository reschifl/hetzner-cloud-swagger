package de.reschif.hetzner.cloud.springexample.it.read;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hetzner.cloud.api.client.model.ServerTypesResponse;
import de.reschif.hetzner.cloud.springexample.it.AbstractServerTypeApiIT;

public class ServerTypeApiReadIT extends AbstractServerTypeApiIT {

  private static final Logger LOG = LoggerFactory.getLogger(ServerTypeApiReadIT.class);

  @Test
  public void testServerTypesGetAll() {

    ServerTypesResponse serverTypesResponse = serverTypeApi.serverTypesGet(null);
    LOG.info("serverTypesResponse=[{}]", serverTypesResponse);
    assertNotNull(serverTypesResponse);

  }
}
