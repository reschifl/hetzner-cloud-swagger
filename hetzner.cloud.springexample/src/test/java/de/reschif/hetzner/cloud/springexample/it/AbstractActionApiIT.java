package de.reschif.hetzner.cloud.springexample.it;

import org.springframework.beans.factory.annotation.Autowired;
import com.hetzner.cloud.api.ActionApi;

public abstract class AbstractActionApiIT extends AbstractApiIT {

  @Autowired
  protected ActionApi actionApi;

}
