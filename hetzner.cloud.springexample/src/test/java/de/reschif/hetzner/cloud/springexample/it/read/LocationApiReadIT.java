package de.reschif.hetzner.cloud.springexample.it.read;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hetzner.cloud.api.client.model.Location;
import com.hetzner.cloud.api.client.model.LocationResponse;
import com.hetzner.cloud.api.client.model.LocationsResponse;
import de.reschif.hetzner.cloud.springexample.it.AbstractLocationApiIT;

public class LocationApiReadIT extends AbstractLocationApiIT {

  private static final Logger LOG = LoggerFactory.getLogger(LocationApiReadIT.class);

  private Location getLocation() {
    LocationsResponse locationsResponse = locationApi.locationsGet(null);
    return locationsResponse.getLocations().get(0);
  }

  @Test
  public void testLocationsGetAll() {

    LocationsResponse locationsResponse = locationApi.locationsGet(null);
    LOG.info("locationsResponse=[{}]", locationsResponse);
    assertNotNull(locationsResponse);

  }

  @Test
  public void testLocationsGetWithName() {

    Location location1 = getLocation();
    String name = location1.getName();
    assertNotNull(name);

    LocationsResponse locationsResponse = locationApi.locationsGet(name);
    LOG.info("locationsResponse=[{}]", locationsResponse);
    assertNotNull(locationsResponse);

    List<Location> locationsList = locationsResponse.getLocations();
    LOG.info("locationsList=[{}]", locationsList);
    assertNotNull(locationsList);
    assertTrue(locationsList.size() == 1);

    Location location2 = locationsList.get(0);
    LOG.info("location2=[{}]", location2);
    assertNotNull(location2);
    assertEquals(location1, location2);

  }

  @Test
  public void testLocationsGetId() {

    Location location1 = getLocation();
    BigDecimal id = location1.getId();

    LocationResponse locationResponse = locationApi.locationsIdGet(id);
    LOG.info("locationResponse=[{}]", locationResponse);
    assertNotNull(locationResponse);

    Location location2 = locationResponse.getLocation();
    LOG.info("location2=[{}]", location2);
    assertNotNull(location2);
    assertEquals(location1, location2);

  }

}
