package de.reschif.hetzner.cloud.springexample.it;

import org.springframework.beans.factory.annotation.Autowired;
import com.hetzner.cloud.api.ServerTypeApi;

public abstract class AbstractServerTypeApiIT extends AbstractApiIT {

  @Autowired
  protected ServerTypeApi serverTypeApi;

}
