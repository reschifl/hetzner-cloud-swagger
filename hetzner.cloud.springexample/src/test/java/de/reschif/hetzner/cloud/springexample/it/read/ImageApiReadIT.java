package de.reschif.hetzner.cloud.springexample.it.read;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hetzner.cloud.api.client.model.ImagesResponse;
import de.reschif.hetzner.cloud.springexample.it.AbstractImageApiIT;

public class ImageApiReadIT extends AbstractImageApiIT {

  private static final Logger LOG = LoggerFactory.getLogger(ImageApiReadIT.class);

  @Test
  public void testImagesGetAll() {

    ImagesResponse imagesResponse = imageApi.imagesGet(null, null, null, null);
    LOG.info("imagesResponse=[{}]", imagesResponse);
    assertNotNull(imagesResponse);

  }

}
