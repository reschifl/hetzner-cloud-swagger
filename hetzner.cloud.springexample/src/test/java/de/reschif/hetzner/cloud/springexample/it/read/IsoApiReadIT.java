package de.reschif.hetzner.cloud.springexample.it.read;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hetzner.cloud.api.client.model.ISOsResponse;
import de.reschif.hetzner.cloud.springexample.it.AbstractIsoApiIT;

public class IsoApiReadIT extends AbstractIsoApiIT {

  private static final Logger LOG = LoggerFactory.getLogger(IsoApiReadIT.class);

  // private ISO getLocation() {
  // ISOsResponse iSOsResponse = isoApi.isosGet(null);
  // return iSOsResponse.getIsos().get(0);
  // }

  @Test
  public void testIsosGetAll() {

    ISOsResponse iSOsResponse = isoApi.isosGet(null);
    LOG.info("iSOsResponse=[{}]", iSOsResponse);
    assertNotNull(iSOsResponse);

  }

}
