package de.reschif.hetzner.cloud.springexample.it.write;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hetzner.cloud.api.client.model.Body;
import com.hetzner.cloud.api.client.model.Image;
import com.hetzner.cloud.api.client.model.ImageResponse;
import com.hetzner.cloud.api.client.model.ImagesResponse;
import de.reschif.hetzner.cloud.springexample.it.AbstractImageApiIT;

public class ImageApiWriteIT extends AbstractImageApiIT {

  private static final Logger LOG = LoggerFactory.getLogger(ImageApiWriteIT.class);

  @Test
  public void testImagesIdPut() {

    ImagesResponse imagesResponse =
        imageApi.imagesGet(null, Arrays.asList(Image.TypeEnum.BACKUP.getValue()), null, null);
    LOG.info("imagesResponse=[{}]", imagesResponse);
    assertNotNull(imagesResponse);

    List<Image> images = imagesResponse.getImages();
    LOG.info("images=[{}]", images);
    assertNotNull(images);

    if (images.size() > 0) {
      Image image = images.get(0);
      assertNotNull(image);

      String descriptionOriginal = image.getDescription();
      LOG.info("descriptionOriginal=[{}]", descriptionOriginal);

      String descriptionNew = "TST-" + descriptionOriginal;

      Body body = new Body();
      body.setDescription(descriptionNew);

      imageApi.imagesIdPut(image.getId(), body);

      ImageResponse imageResponse = imageApi.imagesIdGet(image.getId());
      LOG.info("imageResponse=[{}]", imageResponse);
      assertEquals(descriptionNew, imageResponse.getImage().getDescription());

    }

  }

}
