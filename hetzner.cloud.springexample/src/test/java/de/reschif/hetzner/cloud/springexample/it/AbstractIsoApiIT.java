package de.reschif.hetzner.cloud.springexample.it;

import org.springframework.beans.factory.annotation.Autowired;
import com.hetzner.cloud.api.IsoApi;

public abstract class AbstractIsoApiIT extends AbstractApiIT {

  @Autowired
  protected IsoApi isoApi;

}
