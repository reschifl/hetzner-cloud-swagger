package de.reschif.hetzner.cloud.springexample.it;

import org.springframework.beans.factory.annotation.Autowired;
import com.hetzner.cloud.api.LocationApi;

public abstract class AbstractLocationApiIT extends AbstractApiIT {

  @Autowired
  protected LocationApi locationApi;

}
