package de.reschif.hetzner.cloud.springexample.it.read;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hetzner.cloud.api.client.model.ActionsResponse;
import de.reschif.hetzner.cloud.springexample.it.AbstractActionApiIT;

public class ActionApiReadIT extends AbstractActionApiIT {

  private static final Logger LOG = LoggerFactory.getLogger(ActionApiReadIT.class);

  @Test
  public void testActionsGetAll() {

    ActionsResponse actionsResponse = actionApi.actionsGet(null, null);
    LOG.info("actionsResponse=[{}]", actionsResponse);
    assertNotNull(actionsResponse);

  }

}
