package de.reschif.hetzner.cloud.springexample.it;

import org.springframework.beans.factory.annotation.Autowired;
import com.hetzner.cloud.api.ImageApi;

public abstract class AbstractImageApiIT extends AbstractApiIT {

  @Autowired
  protected ImageApi imageApi;

}
