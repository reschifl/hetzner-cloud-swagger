# hetzner-cloud-swagger

The yaml file contains a currently incomplete swagger definition for the Hetzner cloud api (https://docs.hetzner.cloud/).

It can be viewed directly within the [swagger ui](http://petstore.swagger.io/?url=https://raw.githubusercontent.com/lfischer/hetzner-cloud-swagger/master/hetzner_cloud_api_swagger.yaml).

## How to use it

 get swagger-codegen:

    wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
    

take a look at the help:

    java -jar swagger-codegen-cli.jar help

    java -jar swagger-codegen-cli.jar config-help -l java

or visit https://github.com/swagger-api/swagger-codegen

generate a client:

    java -jar swagger-codegen-cli.jar generate \
        -i ./hetzner_cloud_api_swagger.yaml \
        --api-package com.hetzner.cloud.api \
        --model-package com.hetzner.cloud.api.client.model \
        --invoker-package com.hetzner.cloud.api.client.invoker \
        --group-id de.reschif \
        --artifact-id hetzner-cloud-swagger-api-client \
        --artifact-version 0.0.1-SNAPSHOT \
        -l java \
        --library resttemplate \
        -o spring-swagger-codegen-api-client
